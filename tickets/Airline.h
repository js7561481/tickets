//
//  Airline.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Fare, AirlinesDirectory;

@interface Airline : NSManagedObject

@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSSet *fare;
@property (nonatomic, retain) AirlinesDirectory *airlinesDirectory;
@end

@interface Airline (CoreDataGeneratedAccessors)

- (void)addFareObject:(Fare *)value;
- (void)removeFareObject:(Fare *)value;
- (void)addFare:(NSSet *)values;
- (void)removeFare:(NSSet *)values;

@end

//
//  PickerViewController.h
//  tickets
//
//  Created by Dmitry Varavkin on 15/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickerViewControllerDelegate <NSObject>
@required
- (void)datePickerHasBeenChanged:(NSDate *)date;
- (void)pickerHasBeenChanged:(NSDictionary *)dictionary;
@end

@interface PickerViewController : UIViewController

@property (weak, nonatomic) id <PickerViewControllerDelegate> delegate;

- (void)switchPicker:(id)sender;


@end

//
//  DropDownMenuTableViewController.h
//  tickets
//
//  Created by Dmitry Varavkin on 14/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationRecord.h"

@protocol DropDownMenuTableViewControllerDelegate <NSObject>
@required
- (void)selectedLocationRecord:(LocationRecord *)locationRecord;
@end

@interface DropDownMenuTableViewController : UITableViewController

@property (weak, nonatomic) id<DropDownMenuTableViewControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *items;

- (void)updateRequestForSearchString:(NSString *)searchString;

@end

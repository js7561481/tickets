//
//  LocationRecord.h
//  tickets
//
//  Created by Dmitry Varavkin on 15/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationRecord : NSObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *cityCode;

@end

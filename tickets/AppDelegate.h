//
//  AppDelegate.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/13/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Entities.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// Core Data helpers
- (void)resetCache;
- (NSObject *)newObjectForEntity:(NSString *)entity;
- (NSUInteger)fetchNumberOfObjectsFromEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate;
- (id)fetchObjectFromEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end


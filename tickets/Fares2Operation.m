//
//  Fares2Operation.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/16/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "Fares2Operation.h"
#import "AppDelegate.h"

// string contants found in the feed
static NSString *kCode     = @"Code";
static NSString *kFares   = @"Fares";
static NSString *kTotalAmount   = @"TotalAmount";

@interface Fares2Operation ()
@property (nonatomic, strong) NSData *dataToParse;
@end

#pragma mark -

@implementation Fares2Operation

// -------------------------------------------------------------------------------
//	initWithData:
// -------------------------------------------------------------------------------
- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        _dataToParse = data;
    }
    return self;
}

// -------------------------------------------------------------------------------
//	main
//  Entry point for the operation.
// -------------------------------------------------------------------------------
- (void)main
{
    if (self.dataToParse) {
        @autoreleasepool {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

            //parse out the json data
            NSError *error;
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:self.dataToParse options:kNilOptions error:&error];
            [jsonDictionary[@"Airlines"] enumerateObjectsUsingBlock:^(NSDictionary *jsonAirlineObj, NSUInteger idx, BOOL *stop) {
                
                if (![self isCancelled])
                {
                    NSString *code = jsonAirlineObj[kCode];
                    if (code != (id)[NSNull null]) {
                        
                        Airline *airline = (Airline *)[appDelegate newObjectForEntity:@"Airline"];
                        airline.code = code;
                        
                        NSString *stringForPredicate = @"(code == %@)";
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:stringForPredicate, code];
                        AirlinesDirectory *airlinesDirectory = (AirlinesDirectory *)[appDelegate fetchObjectFromEntity:@"AirlinesDirectory" withPredicate:predicate];
                        if (airlinesDirectory) {
                            airline.airlinesDirectory = airlinesDirectory;
                        }
                        
                        NSArray *fares = [jsonAirlineObj valueForKey:kFares];
                        [fares enumerateObjectsUsingBlock:^(NSDictionary *fareObj, NSUInteger idx, BOOL *stop) {
                            Fare *fare = (Fare *)[appDelegate newObjectForEntity:@"Fare"];
                            fare.totalAmount = fareObj[kTotalAmount];
                            [airline addFareObject:fare];
                        }];
                        
                    }
                }
            }];
            
            [appDelegate saveContext];
            
            self.dataToParse = nil;

        }
    }
}

@end

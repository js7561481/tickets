//
//  Fare.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "Fare.h"
#import "Airline.h"


@implementation Fare

@dynamic totalAmount;
@dynamic airline;

@end

//
//  ResultsTableViewController.m
//  tickets
//
//  Created by Dmitry Varavkin on 17/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "ResultsTableViewController.h"
#import "FaresTableViewController.h"
#import "AppDelegate.h"

@interface ResultsTableViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

#pragma mark -

@implementation ResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    _fetchedResultsController = [self fetchedResultsController];

    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    _fetchedResultsController = nil;
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    return numberOfRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    
    // Configure the cell...
    Airline *airline = (Airline *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = airline.airlinesDirectory.title;
    NSNumber *minFare = [[airline.fare allObjects] valueForKeyPath:@"@min.totalAmount"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",minFare];

    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showAllFares"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Airline *airline = (Airline *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        if (airline) {
            NSArray *sortedFares = [self sortArray:[airline.fare allObjects] bySortKey:@"totalAmount" sortAscending:YES];
            FaresTableViewController *faresTableViewController = (FaresTableViewController *)[segue destinationViewController];
            faresTableViewController.fares = sortedFares;
            faresTableViewController.title = ((UITableViewCell *)sender).textLabel.text;
        }
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
     if (_fetchedResultsController != nil)
     {
         return _fetchedResultsController;
     }
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
 
    // --------------------------------------
    // Set up the fetched results controller.
    // --------------------------------------
    
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // Edit the entity name as appropriate.
    NSEntityDescription *fare = [NSEntityDescription entityForName:@"Airline" inManagedObjectContext:[appDelegate managedObjectContext]];
    [fetchRequest setEntity:fare];
    
    // Before change the predicate, do a delete cache
    [NSFetchedResultsController deleteCacheWithName:@"Root"];
    
    //NSAttributeDescription *airlinesDesc = [fare.relationshipsByName objectForKey:@"airline"];
    //[fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObjects:airlinesDesc, nil]];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Set sortDescriptors.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:[appDelegate managedObjectContext]
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:@"Root"];
   
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error])
    {
        // Handle the error appropriately
        NSLog(@"Unresolved error while perform fetch %@, %@", error, [error userInfo]);
    }

    
    if ([[aFetchedResultsController fetchedObjects] count] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ничего не найдено"
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    return aFetchedResultsController;
}

#pragma mark - Sort array by order

- (NSArray *)sortArray:(NSArray *)unsortedArray bySortKey:(NSString *)sortKey sortAscending:(BOOL)sortAscending
{
    NSArray *sortedArray;
    sortedArray = [unsortedArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [a valueForKey:sortKey];
        NSNumber *second = [b valueForKey:sortKey];
        
        if (sortAscending) {
            return [first compare:second];
        }
        else {
            return [second compare:first];
        }
    }];
    
    return sortedArray;
}

@end

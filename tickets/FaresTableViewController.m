//
//  FaresTableViewController.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "FaresTableViewController.h"
#import "Entities.h"

@interface FaresTableViewController ()

@end

@implementation FaresTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    self.fares = nil;
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.fares count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    
    // Configure the cell...
    Fare *fare = (Fare *)[self.fares objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",fare.totalAmount];

    return cell;
}

@end

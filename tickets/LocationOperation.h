//
//  LocationOperation.h
//  tickets
//
//  Created by Dmitry Varavkin on 15/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationOperation : NSOperation

// A block to call when an error is encountered during parsing.
@property (nonatomic, copy) void (^errorHandler)(NSError *error);

// NSArray containing instances for each entry parsed
// from the input data.
// Only meaningful after the operation has completed.
@property (nonatomic, strong, readonly) NSArray *recordList;

// The initializer for this NSOperation subclass.
- (instancetype)initWithData:(NSData *)data;

@end

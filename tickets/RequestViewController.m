//
//  RequestViewController.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/13/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "RequestViewController.h"
#import "ResultsTableViewController.h"
#import "DropDownMenuTableViewController.h"
#import "AirlinesDirectoryOperation.h"
#import "Fares2Operation.h"
#import "LocationRecord.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

// the URLs used for fetching
static NSString *const newRequest2 = @"https://www.anywayanyday.com/api2/NewRequest2/?Route=%@%@%@AD%@CN0IN0SC%@&_Serialize=JSON";
static NSString *const requestState = @"https://www.anywayanyday.com/api2/RequestState/?R=%@&_Serialize=JSON";
static NSString *const fares2 = @"https://www.anywayanyday.com/api2/Fares2/?L=RU&C=RUB&DebugFullNames=true&_Serialize=JSON&R=%@";
static NSString *const getAirlines = @"https://www.anywayanyday.com/Controller/UserFuncs/BackOffice/GetAirlines/";

#pragma mark static functions

static NSString *FormatDateForUser(NSDate *date)
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    return [dateFormat stringFromDate:date];
}

static NSString *FormatDate(NSDate *date)
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"ddMM"];
    return [dateFormat stringFromDate:date];
}

@interface RequestViewController () <UITextFieldDelegate, DropDownMenuTableViewControllerDelegate> {
    int keyboardHeight;
    
    NSString *fdate; // formatted date
    NSString *from;  // start
    NSString *to;    // destination
    NSNumber *ad;    // passengers
    NSString *sc;    // service
    
    NSString *idSynonym;
}
@property (weak, nonatomic) DropDownMenuTableViewController *dropDownMenuTableViewController;

@property (weak, nonatomic) IBOutlet UITextField *fromTextField;
@property (weak, nonatomic) IBOutlet UITextField *toTextField;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UIButton *passengersButton;
@property (weak, nonatomic) IBOutlet UIButton *economyButton;
@property (weak, nonatomic) IBOutlet UIButton *businessButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@property (weak, nonatomic) IBOutlet UIView *dropDownMenuContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropDownMenuContainerVerticalSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropDownMenuContainerHeight;

@property (weak, nonatomic) IBOutlet UIView *datePickerContainer;
@property (weak, nonatomic) MBProgressHUD *hud;

// the queue
@property (nonatomic, strong) NSOperationQueue *queue;
// a network connections to the service
@property (nonatomic, strong) NSURLConnection *urlConnectionNewRequest2;
@property (nonatomic, strong) NSURLConnection *urlConnectionRequestState;
@property (nonatomic, strong) NSURLConnection *urlConnectionFares2;
@property (nonatomic, strong) NSURLConnection *getAirlines;

// a received data will be stored in
@property (nonatomic, strong) NSMutableData *reseivedData;
@end

@implementation RequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dropDownMenuContainer.hidden = YES;
    self.datePickerContainer.hidden = YES;

    // Set delegate
    self.dropDownMenuTableViewController = (DropDownMenuTableViewController *)[[self childViewControllers] firstObject];
    self.dropDownMenuTableViewController.delegate = self;

    self.datePickerViewController = (PickerViewController *)[[self childViewControllers] lastObject];
    self.datePickerViewController.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Customize buttons
    [self.dateButton setTitle:FormatDateForUser([NSDate date]) forState:UIControlStateNormal];
    UIImage *buttonBkgR = [[UIImage imageNamed:@"button_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    UIImage *buttonBkgHR = [[UIImage imageNamed:@"button_background_highlighted"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    [self.dateButton setBackgroundImage:buttonBkgR forState:UIControlStateNormal];
    [self.passengersButton setBackgroundImage:buttonBkgR forState:UIControlStateNormal];
    [self.dateButton setBackgroundImage:buttonBkgHR forState:UIControlStateHighlighted];
    [self.passengersButton setBackgroundImage:buttonBkgHR forState:UIControlStateHighlighted];
    
    self.economyButton.selected = YES;
    UIImage *switchBkgLeftR = [[UIImage imageNamed:@"switch_left_norm"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    UIImage *switchBkgLeftHR = [[UIImage imageNamed:@"switch_left_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    [self.economyButton setBackgroundImage:switchBkgLeftR forState:UIControlStateNormal];
    [self.economyButton setBackgroundImage:switchBkgLeftHR forState:UIControlStateSelected];
    [self.economyButton setBackgroundImage:switchBkgLeftR forState:UIControlStateHighlighted];
    
    self.businessButton.selected = NO;
    UIImage *switchBkgRightR = [[UIImage imageNamed:@"switch_right_norm"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    UIImage *switchBkgRightHR = [[UIImage imageNamed:@"switch_right_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    [self.businessButton setBackgroundImage:switchBkgRightR forState:UIControlStateNormal];
    [self.businessButton setBackgroundImage:switchBkgRightHR forState:UIControlStateSelected];
    [self.businessButton setBackgroundImage:switchBkgRightR forState:UIControlStateHighlighted];
    
    UIImage *searchBkgR = [[UIImage imageNamed:@"search_button_norm"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    UIImage *searchBkgHR = [[UIImage imageNamed:@"search_button_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 30, 10, 30) resizingMode:UIImageResizingModeStretch];
    [self.searchButton setBackgroundImage:searchBkgR forState:UIControlStateNormal];
    [self.searchButton setBackgroundImage:searchBkgHR forState:UIControlStateHighlighted];
    

}

- (void)viewWillAppear:(BOOL)animated
{
    fdate = FormatDate([NSDate date]);
    ad = @(1);
    sc = @"E";
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate resetCache];
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [self.queue cancelAllOperations];
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Keyboard notifications

// Listen for keyboard hide/show notifications so we can properly adjust the table's height
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // The keyboard is showing so resize the textView's height
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardHeight = keyboardRect.size.height;
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    self.dropDownMenuContainerHeight.constant = screenSize.height - self.navigationController.navigationBar.frame.size.height - self.dropDownMenuContainerVerticalSpace.constant - keyboardHeight - [UIApplication sharedApplication].statusBarFrame.size.height - 10;
}

#pragma mark - Text field delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.datePickerContainer.hidden = YES;

    switch (textField.tag) {
        case 10:
            self.dropDownMenuContainerVerticalSpace.constant = 18 + textField.bounds.origin.y + textField.bounds.size.height;
            break;
        case 20:
            self.dropDownMenuContainerVerticalSpace.constant = 52 + textField.bounds.origin.y + textField.bounds.size.height;
            break;
        default:
            break;
    }

    [self.dropDownMenuTableViewController setItems:nil];
    [self.dropDownMenuTableViewController.tableView reloadData];
    [self.dropDownMenuTableViewController updateRequestForSearchString:textField.text];
    self.dropDownMenuContainer.hidden = NO;

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.dropDownMenuContainer.hidden = YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ( [string isEqualToString:@"\n"] ) { // Return key was pressed
        [textField resignFirstResponder];
        return NO;
    }
    
    NSString *searchStr = [textField.text stringByAppendingString:string];
    if ([string length] == 0) {
        searchStr = [searchStr substringToIndex:[searchStr length] - 1];
    }
    
    NSLog(@"textField.text = %@",searchStr);
    if (self.dropDownMenuTableViewController) {
        [self.dropDownMenuTableViewController updateRequestForSearchString:searchStr];
    }
    
    return YES;
}

- (void)selectedLocationRecord:(LocationRecord *)locationRecord {
    if (self.fromTextField.isFirstResponder) {
        self.fromTextField.text = locationRecord.city;
        [self.fromTextField resignFirstResponder];
        from = locationRecord.cityCode;
    } else if (self.toTextField.isFirstResponder) {
        self.toTextField.text = locationRecord.city;
        [self.toTextField resignFirstResponder];
        to = locationRecord.cityCode;
    }
    self.dropDownMenuContainer.hidden = YES;
}

#pragma mark - PickerViewController delegate methods

- (void)datePickerHasBeenChanged:(NSDate *)date
{
    [self.dateButton setTitle:FormatDateForUser(date) forState:UIControlStateNormal];
    fdate = FormatDate(date);
}

- (void)pickerHasBeenChanged:(NSDictionary *)dictionary
{
    [self.passengersButton setTitle:dictionary[@"str"] forState:UIControlStateNormal];
    ad = dictionary[@"value"];
}

#pragma mark - IBActions

- (IBAction)showDatePicker:(id)sender {
    
    self.datePickerContainer.hidden = NO;
    [self.datePickerViewController switchPicker:sender];

}

- (IBAction)switchEconomy:(id)sender {
    
    self.datePickerContainer.hidden = YES;

    self.economyButton.selected = YES;
    self.businessButton.selected = NO;
    
    sc = @"E";
}

- (IBAction)switchBusiness:(id)sender {
    
    self.datePickerContainer.hidden = YES;

    self.economyButton.selected = NO;
    self.businessButton.selected = YES;
    
    sc = @"B";
}

- (IBAction)launchSearch:(id)sender {
    
    self.datePickerContainer.hidden = YES;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSUInteger airlinesCount = [appDelegate fetchNumberOfObjectsFromEntity:@"AirlinesDirectory" withPredicate:nil];
    if (airlinesCount == 0) {
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:getAirlines]];
        _getAirlines = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        
        // Test the validity of the connection object.
        NSAssert(self.getAirlines != nil, @"Failure to create URL connection.");
    } else {
        // Start UrlConnectionNewRequest2.
        [self startUrlConnectionNewRequest2];
    }
    
    // Show in the status bar that network activity is starting.
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    self.hud.labelText = @"Поиск";
}

- (void)startUrlConnectionNewRequest2
{
    NSString *url = [NSString stringWithFormat:newRequest2, fdate, from, to, ad, sc];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    _urlConnectionNewRequest2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    // Test the validity of the connection object.
    NSAssert(self.urlConnectionNewRequest2 != nil, @"Failure to create URL connection.");
    
    // Show in the status bar that network activity is starting.
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

#pragma mark - Error handling

// -------------------------------------------------------------------------------
//	handleError:error
//  Reports any error with an alert which was received from connection or loading failures.
// -------------------------------------------------------------------------------
- (void)handleError:(NSString *)errorMessage
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot Show Location List"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showResults"]) {
        ((ResultsTableViewController *)[segue destinationViewController]).title = [NSString stringWithFormat:@"%@ - %@",self.fromTextField.text,self.toTextField.text];
    }
}

#pragma mark - NSURLConnectionDelegate

// -------------------------------------------------------------------------------
//	connection:didReceiveResponse:response
//  Called when enough data has been read to construct an NSURLResponse object.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.reseivedData = [NSMutableData data];    // start off with new data
}

// -------------------------------------------------------------------------------
//	connection:didReceiveData:data
//  Called with a single immutable NSData object to the delegate, representing the next
//  portion of the data loaded from the connection.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.reseivedData appendData:data];  // append incoming data
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
//  Will be called at most once, if an error occurs during a resource load.
//  No other callbacks will be made after.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (error.code == kCFURLErrorNotConnectedToInternet)
    {
        // if we can identify the error, we can present a more precise message to the user.
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"No Connection Error"};
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
                                                         code:kCFURLErrorNotConnectedToInternet
                                                     userInfo:userInfo];
        NSString *errorMessage = [noConnectionError localizedDescription];
        [self handleError:errorMessage];
    }
    else
    {
        // otherwise handle the error generically
        NSString *errorMessage = [error localizedDescription];
        [self handleError:errorMessage];
    }
    
    // Release our connections if error occured
    if ([connection isEqual:self.urlConnectionNewRequest2]) {
        self.urlConnectionNewRequest2 = nil;
    } else if ([connection isEqual:self.urlConnectionRequestState]) {
        self.urlConnectionRequestState = nil;
        [self.hud hide:YES];
    } else if ([connection isEqual:self.urlConnectionFares2]) {
        self.urlConnectionFares2 = nil;
        [self.hud hide:YES];
    } else if ([connection isEqual:self.getAirlines]) {
        self.getAirlines = nil;
        [self.hud hide:YES];
    }
}

- (void)launchRequestStateConnection {
    NSString *url = [NSString stringWithFormat:requestState, idSynonym];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    _urlConnectionRequestState = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    // Test the validity of the connection object.
    NSAssert(self.urlConnectionRequestState != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

// -------------------------------------------------------------------------------
//	connectionDidFinishLoading:connection
//  Called when all connection processing has completed successfully, before the delegate
//  is released by the connection.
// -------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    if ([connection isEqual:self.urlConnectionNewRequest2]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.urlConnectionNewRequest2 = nil;   // release our connection
        
        //parse out the json data
        NSError *error;
        NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:self.reseivedData options:kNilOptions error:&error];
        self.reseivedData = nil;

        idSynonym = [jsonObj valueForKey:@"IdSynonym"];
        if (idSynonym) {
            [self launchRequestStateConnection];
        }
    } else if ([connection isEqual:self.urlConnectionRequestState]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.urlConnectionRequestState = nil;   // release our connection
        
        //parse out the json data
        NSError *err;
        NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:self.reseivedData options:kNilOptions error:&err];
        self.reseivedData = nil;

        NSNumber *completed = [jsonObj valueForKey:@"Completed"];
        NSString *error = [jsonObj valueForKey:@"Error"];
        if ([completed intValue] < 100 && error == (id)[NSNull null]) {
            [self performSelector:@selector(launchRequestStateConnection) withObject:nil afterDelay:2];
            self.hud.progress = [completed floatValue]/100;
        } else if (error == (id)[NSNull null]) {
            self.hud.progress = 1;
    
            NSString *url = [NSString stringWithFormat:fares2, idSynonym];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            _urlConnectionFares2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
            
            // Test the validity of the connection object.
            NSAssert(self.urlConnectionFares2 != nil, @"Failure to create URL connection.");
            
            // show in the status bar that network activity is starting
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        } else {
            [self handleError:error];
        }
    } else if ([connection isEqual:self.urlConnectionFares2]) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.urlConnectionFares2 = nil;   // release our connection
        
        // Create the queue to run our ParseOperation
        self.queue = [[NSOperationQueue alloc] init];
        
        // Create an LocationOperation (NSOperation subclass) to parse the data
        // so that the UI is not blocked
        Fares2Operation *parser = [[Fares2Operation alloc] initWithData:self.reseivedData];
        
        parser.errorHandler = ^(NSError *parseError) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *errorMessage = [parseError localizedDescription];
                [self handleError:errorMessage];
            });
        };
        
        parser.completionBlock = ^(void) {
            // The completion block may execute on any thread.  Because operations
            // involving the UI are about to be performed, make sure they execute
            // on the main thread.
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.hud hide:YES];

                // Show the list of Airlines.
                [self performSegueWithIdentifier:@"showResults" sender:nil];
            });

            // we are finished with the queue and our ParseOperation
            self.queue = nil;
        };
        
        [self.queue addOperation:parser]; // this will start the "ParseOperation"
        
        
        // ownership of reseivedData has been transferred to the parse operation
        // and should no longer be referenced in this thread
        self.reseivedData = nil;

    } else if ([connection isEqual:self.getAirlines]) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.getAirlines = nil;   // release our connection
        
        // create the queue to run our ParseOperation
        self.queue = [[NSOperationQueue alloc] init];
        
        // create an LocationOperation (NSOperation subclass) to parse the data
        // so that the UI is not blocked
        AirlinesDirectoryOperation *parser = [[AirlinesDirectoryOperation alloc] initWithData:self.reseivedData];
        
        parser.errorHandler = ^(NSError *parseError) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *errorMessage = [parseError localizedDescription];
                [self handleError:errorMessage];
            });
        };
        
        parser.completionBlock = ^(void) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Start UrlConnectionNewRequest2.
                [self startUrlConnectionNewRequest2];
            });

            // we are finished with the queue and our ParseOperation
            self.queue = nil;
        };
        
        [self.queue addOperation:parser]; // this will start the "ParseOperation"
        
        
        // ownership of reseivedData has been transferred to the parse operation
        // and should no longer be referenced in this thread
        self.reseivedData = nil;
    }
}

@end

//
//  Airline.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "Airline.h"
#import "Fare.h"
#import "AirlinesDirectory.h"


@implementation Airline

@dynamic code;
@dynamic fare;
@dynamic airlinesDirectory;

@end

//
//  PickerViewController.m
//  tickets
//
//  Created by Dmitry Varavkin on 15/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "PickerViewController.h"

@interface PickerViewController() {
     NSArray *_pickerData;
}

@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet UIPickerView *picker;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

#pragma mark -

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Configure UIDatePicker
    NSDateComponents *comps = [[NSDateComponents alloc] init];

    [comps setYear:1];
    [comps setDay:-1];
    NSDate *maxDate = [[NSCalendar currentCalendar] dateByAddingComponents:comps toDate:[NSDate date] options:0];

    self.datePicker.minimumDate = [NSDate date];
    self.datePicker.maximumDate = maxDate;
    self.datePicker.hidden = YES;
    
    // Configure UIPickerView
    _pickerData = @[@"1 Взрослый", @"2 Взрослых", @"3 Взрослых", @"4 Взрослых", @"5 Взрослых", @"6 Взрослых"];
    self.picker.hidden = YES;

}

- (void)switchPicker:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case 1:
            self.datePicker.hidden = NO;
            self.picker.hidden = YES;

            break;
        case 2:
            self.datePicker.hidden = YES;
            self.picker.hidden = NO;

            break;
        default:
            break;
    }
}

#pragma mark - UIPickerView datasourse

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return (int)_pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

#pragma mark - UIPickerView delegate

// Capture the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    [self.delegate pickerHasBeenChanged:@{@"value":@(row+1),@"str":_pickerData[row]}];
}

#pragma mark - Actions

- (IBAction)datePickerControllerHasBeenChanged:(UIDatePicker *)controller {
    if ([self.delegate respondsToSelector:@selector(datePickerHasBeenChanged:)]) {
        [self.delegate datePickerHasBeenChanged:controller.date];
    }
}

@end

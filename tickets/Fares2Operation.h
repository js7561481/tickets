//
//  Fares2Operation.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/16/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fares2Operation : NSOperation

// A block to call when an error is encountered during parsing.
@property (nonatomic, copy) void (^errorHandler)(NSError *error);

// The initializer for this NSOperation subclass.
- (instancetype)initWithData:(NSData *)data;

@end

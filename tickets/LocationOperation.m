//
//  LocationOperation.m
//  tickets
//
//  Created by Dmitry Varavkin on 15/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "LocationOperation.h"
#import "LocationRecord.h"

// string contants found in the RSS feed
static NSString *kCode     = @"Code";
static NSString *kCity   = @"City";
static NSString *kCountry   = @"Country";
static NSString *kData = @"Data";
static NSString *kCityCode  = @"CityCode";

@interface LocationOperation ()

// Redeclare recordList so we can modify it within this class
@property (nonatomic, strong) NSArray *recordList;

@property (nonatomic, strong) NSData *dataToParse;
@property (nonatomic, strong) NSMutableArray *workingArray;
@property (nonatomic, strong) LocationRecord *workingEntry;

@end

#pragma mark -

@implementation LocationOperation

// -------------------------------------------------------------------------------
//	initWithData:
// -------------------------------------------------------------------------------
- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        _dataToParse = data;
    }
    return self;
}

// -------------------------------------------------------------------------------
//	main
//  Entry point for the operation.
// -------------------------------------------------------------------------------
- (void)main
{
    if (self.dataToParse) {
        @autoreleasepool {
            _workingArray = [NSMutableArray array];
            
            //parse out the json data
            NSError *error;
            NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:self.dataToParse options:kNilOptions error:&error];
            
            NSArray *recordsArray = [jsonObj valueForKey:@"Array"];
            
            [recordsArray enumerateObjectsUsingBlock:^(id jsonObj, NSUInteger idx, BOOL *stop) {
                
                if ([jsonObj valueForKey:@"Airport"] == (id)[NSNull null]) { // filter only cities
                    LocationRecord *workingEntry = [[LocationRecord alloc] init];
                    
                    // Code
                    NSString *code = [jsonObj valueForKey:kCode];
                    workingEntry.code = (code == (id)[NSNull null]) ? @"":code;
                    // City
                    NSString *city = [jsonObj valueForKey:kCity];
                    if (city == (id)[NSNull null]) city = @"";
                    workingEntry.city = city;
                    // Country
                    NSString *country = [jsonObj valueForKey:kCountry];
                    if (country == (id)[NSNull null]) country = @"";
                    workingEntry.country = country;
                    // City
                    NSString *data = [jsonObj valueForKey:kData];
                    if (data == (id)[NSNull null]) data = @"";
                    workingEntry.data = data;
                    // CityCode
                    NSString *cityCode = [jsonObj valueForKey:kCityCode];
                    if (cityCode == (id)[NSNull null]) cityCode = @"";
                    workingEntry.cityCode = cityCode;
                    
                    [self.workingArray addObject:workingEntry];
                }
            }];
            
            if (![self isCancelled])
            {
                // Set recordList to the result of our parsing
                self.recordList = [NSArray arrayWithArray:self.workingArray];
            }
            
            self.workingArray = nil;
            self.dataToParse = nil;
        }
    }
}

@end

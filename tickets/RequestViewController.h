//
//  RequestViewController.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/13/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewController.h"

@interface RequestViewController : UIViewController <PickerViewControllerDelegate>

@property (weak, nonatomic) PickerViewController *datePickerViewController;

- (void)datePickerHasBeenChanged:(NSDate *)date;


@end


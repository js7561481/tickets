//
//  AirlinesDirectoryOperation.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "AirlinesDirectoryOperation.h"
#import "AppDelegate.h"

// string contants found in the feed
static NSString *kCode     = @"Code";
static NSString *kName   = @"Name";

@interface AirlinesDirectoryOperation ()
@property (nonatomic, strong) NSData *dataToParse;
@end

#pragma mark -

@implementation AirlinesDirectoryOperation

// -------------------------------------------------------------------------------
//	initWithData:
// -------------------------------------------------------------------------------
- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        _dataToParse = data;
    }
    return self;
}

// -------------------------------------------------------------------------------
//	main
//  Entry point for the operation.
// -------------------------------------------------------------------------------
- (void)main
{
    if (self.dataToParse) {
        
        @autoreleasepool {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

            //parse out the json data
            NSError *error;
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:self.dataToParse options:kNilOptions error:&error];
            [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *jsonObj, NSUInteger idx, BOOL *stop) {
                if (![self isCancelled])
                {
                    NSString *code = jsonObj[kCode];
                    if (code != (id)[NSNull null]) {
                        
                        AirlinesDirectory *airline = (AirlinesDirectory *)[appDelegate newObjectForEntity:@"AirlinesDirectory"];
                        airline.code = code;
                        
                        NSString *title = jsonObj[kName];
                        airline.title = (title != (id)[NSNull null])?title:@"";
                    }
                }
            }];
            
            
            [appDelegate saveContext];
            
            self.dataToParse = nil;
        }
    }
}

@end

//
//  FaresTableViewController.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaresTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *fares;

@end

//
//  DropDownMenuTableViewController.m
//  tickets
//
//  Created by Dmitry Varavkin on 14/07/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "DropDownMenuTableViewController.h"
#import "LocationOperation.h"

// This framework was imported so we could use the kCFURLErrorNotConnectedToInternet error code.
#import <CFNetwork/CFNetwork.h>

#pragma mark static constants

// the http URL used for fetching
static NSString *const airportFinderRequest = @"https://www.anywayanyday.com/AirportNames/?language=RU&filter=%@&_Serialize=JSON";

#pragma mark static functions

static NSString *WrapSpecial(NSString *str) // Wrap special characters with UTF8 code.
{
    return [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@interface DropDownMenuTableViewController ()

// the queue to run our "LocationOperation"
@property (nonatomic, strong) NSOperationQueue *queue;
// a network connection to the AirportFinder service
@property (nonatomic, strong) NSURLConnection *urlConnection;
// a received data will be stored in
@property (nonatomic, strong) NSMutableData *reseivedData;


@end

@implementation DropDownMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Customize the table
    [self.tableView setBackgroundView:nil];
    [[self tableView] setSeparatorColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_sections_separator_dotted"]]];
    
    self.view.layer.cornerRadius = 5; // if you like rounded corners
    self.tableView.layer.cornerRadius = 5; // if you like rounded corners
}

- (void)didReceiveMemoryWarning {
    [self.queue cancelAllOperations];
    [super didReceiveMemoryWarning];
}

- (void)updateRequestForSearchString:(NSString *)searchString
{
    if ([searchString length] >= 2) {
        NSString *url = [NSString stringWithFormat:airportFinderRequest, WrapSpecial(searchString)];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        _urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        
        // Test the validity of the connection object. The most likely reason for the connection object
        // to be nil is a malformed URL, which is a programmatic error easily detected during development
        // If the URL is more dynamic, then you should implement a more flexible validation technique, and
        // be able to both recover from errors and communicate problems to the user in an unobtrusive manner.
        //
        NSAssert(self.urlConnection != nil, @"Failure to create URL connection.");
        
        // show in the status bar that network activity is starting
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.items count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    
    // Configure the cell...
    LocationRecord *locationRecord =  self.items[indexPath.row];
    cell.textLabel.text = locationRecord.city;
    cell.detailTextLabel.text = locationRecord.country;

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate selectedLocationRecord:self.items[indexPath.row]];
    
}

#pragma mark - Error handling

// -------------------------------------------------------------------------------
//	handleError:error
//  Reports any error with an alert which was received from connection or loading failures.
// -------------------------------------------------------------------------------
- (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot Show Location List"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

// The following are delegate methods for NSURLConnection.

#pragma mark - NSURLConnectionDelegate

// -------------------------------------------------------------------------------
//	connection:didReceiveResponse:response
//  Called when enough data has been read to construct an NSURLResponse object.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.reseivedData = [NSMutableData data];    // start off with new data
}

// -------------------------------------------------------------------------------
//	connection:didReceiveData:data
//  Called with a single immutable NSData object to the delegate, representing the next
//  portion of the data loaded from the connection.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.reseivedData appendData:data];  // append incoming data
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
//  Will be called at most once, if an error occurs during a resource load.
//  No other callbacks will be made after.
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (error.code == kCFURLErrorNotConnectedToInternet)
    {
        // if we can identify the error, we can present a more precise message to the user.
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"No Connection Error"};
        NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
                                                         code:kCFURLErrorNotConnectedToInternet
                                                     userInfo:userInfo];
        [self handleError:noConnectionError];
    }
    else
    {
        // otherwise handle the error generically
        [self handleError:error];
    }
    
    self.urlConnection = nil;   // release our connection
}

// -------------------------------------------------------------------------------
//	connectionDidFinishLoading:connection
//  Called when all connection processing has completed successfully, before the delegate
//  is released by the connection.
// -------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.urlConnection = nil;   // release our connection
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // create the queue to run our ParseOperation
    self.queue = [[NSOperationQueue alloc] init];
    
    // create an LocationOperation (NSOperation subclass) to parse the data
    // so that the UI is not blocked
    LocationOperation *parser = [[LocationOperation alloc] initWithData:self.reseivedData];
    
    parser.errorHandler = ^(NSError *parseError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self handleError:parseError];
        });
    };
    
    // Referencing parser from within its completionBlock would create a retain cycle.
    __weak LocationOperation *weakParser = parser;
    
    parser.completionBlock = ^(void) {
        if (weakParser.recordList) {
            
            self.items = weakParser.recordList;

            // The completion block may execute on any thread.  Because operations
            // involving the UI are about to be performed, make sure they execute
            // on the main thread.
            dispatch_async(dispatch_get_main_queue(), ^{
                // tell our table view to reload its data, now that parsing has completed
                [self.tableView reloadData];
            });
        }
        
        // we are finished with the queue and our ParseOperation
        self.queue = nil;
    };
    
    [self.queue addOperation:parser]; // this will start the "ParseOperation"
    
    // ownership of reseivedData has been transferred to the parse operation
    // and should no longer be referenced in this thread
    self.reseivedData = nil;
}

@end

//
//  AirlinesDirectory.h
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Airline;

@interface AirlinesDirectory : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) Airline *airline;

@end

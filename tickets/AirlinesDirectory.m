//
//  AirlinesDirectory.m
//  tickets
//
//  Created by Varavkin Dmitry on 7/19/15.
//  Copyright (c) 2015 dvaravkin. All rights reserved.
//

#import "AirlinesDirectory.h"
#import "Airline.h"


@implementation AirlinesDirectory

@dynamic title;
@dynamic code;
@dynamic airline;

@end
